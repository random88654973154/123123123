package all.web;

import all.model.User;
import all.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
public class ActionsController {

    @Autowired
    private AuthService authService;

    @GetMapping("/start")
    public HttpEntity start(@CookieValue(name = "session-id") String sessionId, HttpServletResponse response) throws IOException {
        User user = authService.getUser(sessionId);
        if (user != null) {
            // TODO: add code to track time
            response.sendRedirect("/actions.html");
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            response.sendRedirect("/index.html");
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/stop")
    public HttpEntity stop(@CookieValue(name = "session-id") String sessionId, HttpServletResponse response) throws IOException {
        User user = authService.getUser(sessionId);
        if (user != null) {
            String data = user.getName() + "worked ";
            return new ResponseEntity<>(data, HttpStatus.OK);
        } else {
            response.sendRedirect("/index.html");
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }
}
