package all.service;

import all.model.User;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepository {
    public User getUserByName(String name) {
        return new User(name, DigestUtils.sha256Hex("321"));
    }
}

/*
user
id, name, hash-pass
0 sasha hash
1 dima hash2

user-time
id user-id time        type
-    0      123416578    start
-    1      123416583    start
-    0      123416590    stop

select id from user where user.name = ? left join user-time on user-id=id
select * from user-time where id = (select id from user where name = ?)

user
id, name, hash-pass, time-sequence
0, sasha, hash,      |123456, 123460, 123545|
*/