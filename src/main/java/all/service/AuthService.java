package all.service;

import all.model.User;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class AuthService {

    @Autowired
    private UserRepository userRepository;

    private static final Map<String, User> userIds = new ConcurrentHashMap<>();

    public Optional<String> login(User user) {
        User dbUser = userRepository.getUserByName(user.getName());

        String dbPassword = dbUser.getPassword();
        String password = DigestUtils.sha256Hex(user.getPassword());

        if (dbPassword.equals(password)) {
            String sessionId = new BigInteger(128, new Random()).toString(16);
            userIds.put(sessionId, dbUser);
            return Optional.of(sessionId);
        } else {
            return Optional.empty();
        }
    }

    public User getUser(String sessionId) {
        return userIds.get(sessionId);
    }
}
